define([
    // Defaults
    "jquery",
    "angular",
    "angularSanitize"
], function($){
    "use strict";

    var App = {

        init: function(MainApp) {
            // init App
            // ------------------------------------------------------------------------

            MainApp.keepoApp.factory("feeds", function ($http, $q, accessToken) {
                return {
                    get: function (page) {
                        if (page == undefined) page = 1;
                        var feed_url = MainApp.baseURL + 'api/v1/feed/latest?page=' + page;
                        // get the token to access api
                        return accessToken.get().then(
                            function (token) {
                                // token received, now get the feeds data
                                return $http.get(feed_url, token).then(
                                    function (response) {
                                        return response.data;
                                    },
                                    function (response) {
                                        return $q.reject(response.data);
                                    }
                                );
                            }
                        );

                    }
                }
            });

            MainApp.keepoApp.directive('body', function ($document, $window, feeds) {
                return {
                    restrict: "E",
                    link: function (scope, element, attrs) {
                        var visibleHeight = window.innerHeight;
                        var threshold = window.innerHeight / 3;

                        $document.bind("scroll", function () {
                            var scrollableHeight = element.prop('scrollHeight');
                            var hiddenContentHeight = scrollableHeight - visibleHeight;

                            if (hiddenContentHeight - $window.pageYOffset <= threshold) {
                                // Scroll is almost at the bottom. Loading more rows
                                if (!scope.loadNew) {
                                    scope.loadNew = true;
                                    var page = 2; // first loaded data
                                    if (scope.feeds != undefined) {
                                        page = (scope.feeds.length / 20) + 2;
                                    }
                                    feeds.get(page).then(function (data) {
                                        if (scope.feeds == undefined) {
                                            scope.feeds = data;
                                        } else {
                                            scope.feeds = scope.feeds.concat(data);
                                        }
                                        scope.loadNew = false;
                                    }, function (data) {
                                        console.log("error when loading new feeds");
                                        // fail to load, reset the hook
                                        scope.loadNew = false;
                                    })
                                }
                            }
                        });
                    }
                };
            });

            MainApp.keepoApp.directive("adsFeed", function () {
                return {
                    restrict: "E",
                    replace: true,
                    templateUrl: "feedAds"
                }
            });

            MainApp.keepoApp.directive("pageFeed", function () {
                return {
                    restrict: "E",
                    replace: true,
                    templateUrl: "feedTemplate",
                    controller: ['$scope', 'feeds', function ($scope, feeds) {
                        $scope.feeds = [];
                        $scope.loadNew = false;
                    }]
                }
            });

            // start up the module
            MainApp.keepoApp.run();
            angular.bootstrap(document.querySelector("html"), ["keepoApp"]);
        }
    };

    return App;
});